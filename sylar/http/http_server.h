#ifndef __HTTP_SERVER_H__
#define __HTTP_SERVER_H__
#include"http_session.h"
#include"sylar/tcp_server.h"
#include"servlet.h"


namespace sylar{
namespace http{
class HttpServer: public TcpServer{
public:
    using ptr = std::shared_ptr<HttpServer>;
    HttpServer(bool keeplive=true
              ,sylar::IOManager* io_woker = sylar::IOManager::GetThis()
              ,sylar::IOManager* accept_worker = sylar::IOManager::GetThis());

    ServletDispatch::ptr getServletDispatch() const{return m_dispatch;}
    void setServletDispatch(ServletDispatch::ptr v){m_dispatch = v;}

    //保存客户端上传的图片
    void downloadPic(HttpRequest::ptr req);
    
protected:
    // 每accept一个sock执行一次，sock的回调
    void handleClient(Socket::ptr client) override;

private:
    bool m_isKeeplive;   // 长连接，一个sock支持多个请求的连接和发送
    ServletDispatch::ptr m_dispatch;
};


}
}





#endif