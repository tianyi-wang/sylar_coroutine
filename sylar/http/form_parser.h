#ifndef __FORM_PARSER_H__
#define __FORM_PARSER_H__

#include<string>
#include<memory>
#include<vector>

namespace sylar{

class FormItem{

friend class FormDataParser;
private:
    std::string _name;                           ///< 表单元的key
    std::string _fileName;                       ///< 表单元为文件时，具有文件名
    std::string _contentType;                    ///< 表单元该部分的类型
    std::shared_ptr<std::string> _content;       ///< 指request中的表单内容
    int _dataStart;                              ///< 属于本单元素的内容的起始下标
    int _dataLength;                             ///< 属于本单元素的内容的长度

public:
    /**
     * MultipartContentElement对象只能由MultipartContentPars生成
     * 将构造函数的访问权设置为private，防止外部创建
     * @param name 表单元素的名
     * @param fileName 文件时fileName不为空
     * @param contentType 类型
     * @param content 指向表单数据的指针
     * @param start 本表单元素的起始位置
     * @param length 本表单元素的数据长度
     * @return MultipartContentElement对象
     */
    FormItem(const std::string name, 
        const std::string fileName, const std::string contentType,
        const std::shared_ptr<std::string> content, 
        const int start, const int length);

    inline std::string getFileName() const { return _fileName; }
    inline std::string getName() const { return _name; }
    inline std::string getContenType() const {return _contentType; }
    inline bool isFile() const {return !_fileName.empty(); }
    /**
     * 获取具体的内容，不返回指向原始内容的指针
     * 而是复制内容，防止外部对请求作出更改，影响到同一表单中同元素的内容
     * @return 新复制的内容的指针
     */
    std::shared_ptr<std::string>  getContent() {return _content;};
};

class FormDataParser{

private:
    std::shared_ptr<std::string> _data; ///< 指向表单数据的针
    std::string _boundary;              ///< 不同元素的分割符串

    bool _lastBoundaryFound;            ///< 是否找到了最后边界
    int _pos;                           ///< 当前解析到的位置
    int _lineStart;                     ///< 当前行的起始位置
    int _lineLength;                    ///< 当前行的长度

    std::string _partName;              ///< 当前元素的名
    std::string _partFileName;          ///< 当前元素的文件名
    std::string _partContentType;       ///< 当前元素的类型
    int _partDataStart;                 ///< 当前元素的数据表单中的起始位置
    int _partDataLength;                ///< 当前元素的数据长度

public:

    FormDataParser(const std::shared_ptr<std::string> data, 
        const int pos, std::string &boundary):_data(data)
        , _pos(pos), _boundary(boundary) {}
        
    /**
     * 调用parse函数后，才会执行对应的解析操作，
     * @return 指向由FormItem组成的vector的unique_ptr
     */
    std::vector<FormItem> parse();  

    std::shared_ptr<std::string> getData(){ return _data;}

    int32_t getlines(){

    }

private:
    /**
     * 解析表单数据的头部，即紧跟着boundary后面的一行
     */
    void parseHeaders();
    /**
     * 解析表单元素中的具体数据
     */
    void parseFormData();
    /**
     * 获取下一行的数据，
     * 在此实际上是通过更新类内部的_pos, _lineStart,_lineLength实现的
     * @return 是否成功得到下一行的数据
     */
    bool getNextLine();
    /**
     * 判断是否为边界分割行
     * @return 是边界分割行放回true，否则返回false
     */
    bool atBoundaryLine();
    /**
     * 判断是否到达表单数据的末尾
     */
    inline bool atEndOfData(){
        return _pos >= _data->size() || _lastBoundaryFound;
    }            
    std::string getDispositionValue(
        const std::string source, int pos, const std::string name);
    /**
     * 去除字符串前后的空白字符
     * @return 去除空白字符的字符串
     */
    inline std::string& trim(std::string &s){
        if(s.empty()){ return s; }
        s.erase(0, s.find_first_not_of(" "));
        s.erase(s.find_last_not_of(" ") + 1);
        return s;
    }
};



}


#endif