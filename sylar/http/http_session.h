#ifndef __HTTP_SESSION_H__
#define __HTTP_SESSION_H__

#include "sylar/stream/socket_stream.h"
#include "http.h"
#include "http_parser.h" 

namespace sylar{
namespace http{
/**
 * @brief 服务端
 * 主要功能就是接收http请求和发送http响应
 */
class HttpSession : public SocketStream{
public:
    using ptr = std::shared_ptr<HttpSession>;

    HttpSession(Socket::ptr sock, bool owner=true);

    HttpRequest::ptr recvRequest();
    int sendResponse(HttpResponse::ptr rsp);
private:

};

}
}

#endif