#ifndef __DAEMON_H__
#define __DAEMON_H__

#include<functional>
#include"singleton.h"
#include<unistd.h>    // fork
#include <sys/types.h>
#include <sys/wait.h>   // waitpid

namespace sylar{

struct ProcessInfo {
    /// 父进程id
    pid_t parent_id = 0;
    /// 主进程id
    pid_t main_id = 0;
    /// 父进程启动时间
    uint64_t parent_start_time = 0;
    /// 主进程启动时间
    uint64_t main_start_time = 0;
    /// 主进程重启的次数->说明主进程出现过故障然后进行重启了
    uint32_t restart_count = 0;

    std::string toString() const;
};

using ProcessInfoMgr = sylar::Singleton<ProcessInfo>;

/**
 * @brief 选择使用守护进程的方式启动
 * 
 * @param argc 参数个数
 * @param argv 
 * @param main_cb 启动函数 
 * @param is_daemon 是否选择使用守护进程
 * @return int 
 */
int start_daemon(int argc, char** argv, std::function<int(int argc, char** argv)> main_cb
                ,bool is_daemon);

}



#endif