#ifndef __SYLAR_H__
#define __SYLAR_H__

#include"sylar/config.h"
#include"sylar/thread.h"
#include"sylar/log.h"
#include"sylar/util.h"
#include"sylar/singleton.h"
#include"sylar/macro.h"
#include"sylar/fiber.h"
#include"sylar/schedule.h"
#endif