//
// Created by wty on 22-6-26.
//
#include <ucontext.h>
#include <iostream>
#include"sylar/sylar.h"
#include <memory>

sylar::Logger::ptr logger = SYLAR_LOG_ROOT();

void run_in_fiber(){
    SYLAR_LOG_INFO(logger)<<"run in fiber begin";
    sylar::Fiber::GetThis()->YieldToHold();
    SYLAR_LOG_INFO(logger)<<"run in fiber end";
   sylar::Fiber::GetThis()->YieldToHold();
}

void test_fiber(){

    sylar::Fiber::GetThis();   // 防止没有主协程，默认创建一个主协程
    SYLAR_LOG_INFO(logger) << "main begin";
    sylar::Fiber::ptr fiber(new sylar::Fiber(run_in_fiber));
    fiber->swapIn();
    SYLAR_LOG_INFO(logger) << "main after swapIN";
    fiber->swapIn();
    SYLAR_LOG_INFO(logger) << "main after end";
    fiber->swapIn();
}

struct A{
    std::shared_ptr<int> p = std::make_shared<int>(5);
    void change_1(std::shared_ptr<int> *f){
        p.swap(*f);
        std::cout<<*p<<" "<<p.use_count()<<std::endl;
    }
    void change_2(std::shared_ptr<int> f){
        std::cout<<"befor swap f:"<<*f<<" "<<f.use_count()<<std::endl;
        std::cout<<"befor swap p:"<<*p<<" "<<p.use_count()<<std::endl;
        p.swap(f);
        std::cout<<"after swap f:"<<*f<<" "<<f.use_count()<<std::endl;
        std::cout<<"after swap p:"<<*p<<" "<<p.use_count()<<std::endl;
    }

};

struct task{
    int num=0;
    std::string name = "abc";
};

int main(int argc, char** argv) {

    sylar::Thread::SetName("mainT");

    std::vector<sylar::Thread::ptr> thrs;
    for(int i=0;i<1;++i){
        thrs.push_back(sylar::Thread::ptr(new sylar::Thread(test_fiber, "thread"+std::to_string(i))));
    }

    for(auto i:thrs){
        i->join();
    }


    task t;
    task *p = new task;
    p->num = 5;
    p->name = "wty";
    t = *p;
    t.num = 3;
    std::cout<<p->num<<std::endl;
    std::cout<<t.num<<t.name<<std::endl;
    std::cout<<&t<<" "<<p<<std::endl;

    return 0;
}
