//
// Created by wty on 22-7-12.
//
#include "sylar/sylar.h"
#include "sylar/socket.h"
#include "sylar/address.h"
#include "sylar/iomanager.h"

static sylar::Logger::ptr logger = SYLAR_LOG_ROOT();

void test_socket(){
    sylar::IPAddress::ptr address = sylar::Address::LookupAnyIPAddress("www.baidu.com");
    SYLAR_LOG_INFO(logger)<<address->toString();

    sylar::Socket::ptr sock = sylar::Socket::CreateTCP(address);
    address->setPort(80);
    SYLAR_LOG_INFO(logger)<<address->toString();

    if(!sock->connect(address)){
        SYLAR_LOG_INFO(logger)<<"connect "<<address->toString()<<" failed";
    }

    const char buff[] = "GET / HTTP/1.0\r\n\r\n";
    int rt = sock->send(buff, sizeof(buff));
    if(rt<=0){
        SYLAR_LOG_INFO(logger)<<" send failed";
        return;
    }

    std::string buf;
    buf.resize(4096);
    rt = sock->recv(&buf[0], buf.size());
    if(rt<=0){
        SYLAR_LOG_INFO(logger)<<" recv failed";
        return;
    }
    SYLAR_LOG_INFO(logger) << buf;
}

int main(int argc, char** argv){
    sylar::IOManager iom;
    iom.schedule(test_socket);
    return 0;
}