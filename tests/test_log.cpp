/*
 * @Author: TianyiWang
 * @Date: 2022-06-11 10:17:51
 * @LastEditors: TianyiWang
 * @LastEditTime: 2022-06-12 18:39:32
 * @Description: file content
 * @FilePath: /TinyWebServer/home/wty/sylar_learn/tests/test_log.cpp
 */
//
// Created by wty on 22-6-11.
//
#include"sylar/log.h"
#include<iostream>
#include"sylar/config.h"

void test_log_config(){
    std::cout<< sylar::LoggerMgr::GetInstance()->toYamlString()<<std::endl;
    YAML::Node node = YAML::LoadFile("/home/wty/sylar_learn/bin/conf/log.yml");
    sylar::Config::LoadFromYaml(node);
    std::cout<<"==========================="<<std::endl;
    std::cout<< sylar::LoggerMgr::GetInstance()->toYamlString()<<std::endl;

    /// 测试visit功能
    sylar::Config::Visit([](sylar::ConfigVarBase::ptr var){
        SYLAR_LOG_INFO(SYLAR_LOG_ROOT())<< "name="<<var->getName()
                <<" description=" << var->getDescription()
                <<" typename=" << var->getTypeName()
                <<" value=" << var->toString();
    });
}

void test_log2()
{
    static sylar::Logger::ptr system_log = SYLAR_LOG_NAME("system");    // 创建LogManager构造函数中创建root日志器,所以这里创建了root[stdout]和system[没有输出目的地]日志器
    SYLAR_LOG_INFO(system_log) << "hello system" << std::endl;  // system日志器调用其主日志器，即root进行输出
    std::cout << sylar::LoggerMgr::GetInstance()->toYamlString() << std::endl;
    YAML::Node root = YAML::LoadFile("/home/wty/sylar_learn/bin/conf/log.yml");
    sylar::Config::LoadFromYaml(root);
    std::cout << "=============" << std::endl;
    std::cout << sylar::LoggerMgr::GetInstance()->toYamlString() << std::endl;
    std::cout << "=============" << std::endl;
    std::cout << root << std::endl;
    SYLAR_LOG_INFO(system_log) << "hello system" << std::endl;

    system_log->setFormatter("%d - %m%n");
    SYLAR_LOG_INFO(system_log) << "hello system" << std::endl;
}

void test_log1(){
    sylar::Logger::ptr logger(new sylar::Logger("root"));

    logger->addAppender(sylar::LogAppender::ptr(new sylar::StdoutLogAppender));

    sylar::FileLogAppender::ptr file_appender(new sylar::FileLogAppender("./log.txt"));
    sylar::LogFormatter::ptr fmt(new sylar::LogFormatter("%d%T[%p]%m%n"));
    file_appender->setFormatter(fmt);
    file_appender->setLevel(sylar::LogLevel::ERROR);
    logger->addAppender(file_appender);

    SYLAR_LOG_ERROR(logger)<<"this is a error";
    SYLAR_LOG_WARN(logger)<<"this is a warn";
    SYLAR_LOG_FMT_FATAL(logger, "test fmt log %s %d", "asdasd", 2);

    auto l = sylar::LoggerMgr::GetInstance()->getLogger("root");
    SYLAR_LOG_INFO(l)<<"test Singleton LoggerMgr";
}

static sylar::ConfigVar<int>::ptr g_tcp_connect_timeout =
        sylar::Config::Lookup("tcp.connect.timeout", 5000, "tcp connect timeout");
sylar::Logger::ptr logger = SYLAR_LOG_ROOT();
static uint64_t s_connect_timeout = -1;
void test_listener(){

    s_connect_timeout = g_tcp_connect_timeout->getValue();
    SYLAR_LOG_INFO(logger)<<"s_connect_time_out: " << s_connect_timeout;
    g_tcp_connect_timeout->addListener([](const int& old_value, const int& new_value){
        SYLAR_LOG_INFO(logger) << "tcp connect timeout changed from "
                                 << old_value << " to " << new_value;
        s_connect_timeout = new_value;
    });
}

void change_s_connect(){
    g_tcp_connect_timeout->setValue(3000);
    SYLAR_LOG_INFO(logger)<<"s_connect_time_out: " << s_connect_timeout;
}

int main(){

    test_listener();
    change_s_connect();

    return 0;
}