#include"sylar/tcp_server.h"
#include"sylar/iomanager.h"
#include<vector>
sylar::Logger::ptr logger = SYLAR_LOG_ROOT();

void run(){
    auto ipv4_addr = sylar::Address::LookupAny("0.0.0.0:8000");
    //auto unix_addr = sylar::UnixAddress::ptr(new sylar::UnixAddress("/tmp/unix_addr1"));
    // SYLAR_LOG_INFO(logger)<<*addr<<" - "<<*unix_addr;
    std::vector<sylar::Address::ptr> addrs;
    addrs.push_back(ipv4_addr);

    sylar::TcpServer::ptr tcp_server(new sylar::TcpServer);
    std::vector<sylar::Address::ptr> fails;
    remove("/tmp/unix_addr1");
    while(!tcp_server->bind(addrs, fails)){
        sleep(2);
    }
    tcp_server->start();
}

int main(int argc, char** argv){
    sylar::IOManager accept_worker(2, true, "accept");
    sylar::IOManager io_worker(2, false, "io_worker");
    io_worker.schedule(run);


//    auto ipv4_addr = sylar::Address::LookupAny("0.0.0.0:8000");
//    //auto unix_addr = sylar::UnixAddress::ptr(new sylar::UnixAddress("/tmp/unix_addr1"));
//    // SYLAR_LOG_INFO(logger)<<*addr<<" - "<<*unix_addr;
//    std::vector<sylar::Address::ptr> addrs;
//    addrs.push_back(ipv4_addr);
//
//    sylar::TcpServer::ptr tcp_server(new sylar::TcpServer(&io_worker, &io_worker, &accept_worker));
//    std::vector<sylar::Address::ptr> fails;
//    remove("/tmp/unix_addr1");
//    while(!tcp_server->bind(addrs, fails)){
//        sleep(2);
//    }
//    tcp_server->start();

}