//
// Created by wty on 22-6-14.
//
#include"sylar/config.h"
#include"sylar/log.h"
#include<yaml-cpp/yaml.h>
#include<fstream>
#include<vector>


sylar::ConfigVar<int>::ptr g_int_value_config = sylar::Config::Lookup("system.port", (int)8080, "system port");
sylar::ConfigVar<float>::ptr g_float_value_config = sylar::Config::Lookup("system.port", (float)8080, "system port");
sylar::ConfigVar<std::string>::ptr log_config(new sylar::ConfigVar<std::string>("logs.name", "root11", "system port"));
sylar::ConfigVar<std::vector<int>>::ptr g_int_vec_value_config = sylar::Config::Lookup("system.int_vec", std::vector<int>{1,2}, "system int vec");
sylar::ConfigVar<std::list<int>>::ptr g_int_list_value_config = sylar::Config::Lookup("system.int_list", std::list<int>{1,2}, "system int list");

sylar::ConfigVar<std::map<std::string, std::vector<int>>>::ptr g_string_map_value_config = \
                                                sylar::Config::Lookup("system.string_vector", std::map<std::string, std::vector<int>>{{"s1",{1,2,3}},{"s2", {1,3,4}}}, "system int list");

void test_yaml(){
    std::ifstream file("/home/wty/sylar_learn/bin/conf/test.yml");
    YAML::Node root = YAML::Load(file);
    std::cout<<root<<std::endl;
    
    if(root["logs"].IsMap()){
        std::cout<<"root[\"logs\"] is map"<<std::endl;
    }
    if(root["log_1"].IsSequence()){
        std::cout<<"log_1 is sequence"<<std::endl;
    }
    std::vector<std::string> lis = root["log_1"].as<std::vector<std::string>>();
    for(auto i:lis){
        std::cout<<" "<<i;
    }
}

void test_Config(){
    sylar::Config config;

    YAML::Node node = YAML::LoadFile("/home/wty/sylar_learn/bin/conf/log.yml");
    config.LoadFromYaml(node);
    SYLAR_LOG_INFO(SYLAR_LOG_ROOT())<<g_int_value_config->getValue();
    SYLAR_LOG_INFO(SYLAR_LOG_ROOT())<<g_int_value_config->toString();

    auto list = g_int_list_value_config->getValue();
    for(auto i:list){
        std::cout<<i<<std::endl;
    }

    auto m_v = g_string_map_value_config->getValue();
    for(auto it=m_v.begin();it!=m_v.end();it++){
        std::cout<<it->first<<" ";
        for(auto &i:it->second){
            std::cout<<i<<" ";
        }
        std::cout<<std::endl;
    }

}



int main(int argc, char** argv){

    // test_Config();
    std::vector<int> v1{1,2,3,4,5};
    std::stringstream ss;
    YAML::Node node;
    node["a"] = v1;
    if(node["a"].IsSequence()){
        std::cout<<"is sequence"<<std::endl;
    }
    node["b"] = 2;
    ss<<node;
    std::cout<<ss.str();

    test_Config();
    return 0;
}