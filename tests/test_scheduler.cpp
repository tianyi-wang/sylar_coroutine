//
// Created by wty on 22-6-30.
//
#include<thread>
#include"sylar/sylar.h"
sylar::Logger::ptr logger = SYLAR_LOG_ROOT();

void test_fiber(){
	static int s_count = 5;
	SYLAR_LOG_INFO(logger)<<"test in fiber s_count = "<<s_count;
//	sleep(1);
	if(--s_count>=0){
		sylar::Scheduler::GetThis()->schedule(&test_fiber);
	}
}

void fun1(){
    SYLAR_LOG_INFO(logger)<<"fun1";
}

class A{
public:
    void operator()(){
        fun1();
    }
};

void test(){
    std::vector<std::thread*> thread_pool;
    for(int i=0;i<5;i++){
        thread_pool.push_back(new std::thread(A()));
    }
//    for(auto&i : thread_pool){
//        i->join();
//    }
    std::vector<std::thread*> thrs;
    thrs.swap(thread_pool);
    for(auto &i:thrs){
        i->join();
    }
}

int main(int argc, char** argv){
    
    sylar::Scheduler sc(3, false, "test");
    sc.start();

    sc.schedule(&test_fiber);
    SYLAR_LOG_INFO(logger)<<"task num: "<<sc.taskNum();
    sc.stop();
	SYLAR_LOG_INFO(logger)<<"over";


    return 0;
}