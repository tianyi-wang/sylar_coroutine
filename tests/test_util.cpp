#include"sylar/sylar.h"
#include<assert.h>
#include"sylar/Base64.h"
#include<iostream>
#include<string>
#include<fstream>
#include<opencv4/opencv2/dnn.hpp>
#include<opencv2/imgproc.hpp>
#include<opencv2/opencv.hpp>

sylar::Logger::ptr logger = SYLAR_LOG_ROOT();

void test_assert(){
    SYLAR_LOG_INFO(logger)<<sylar::BacktraceToString(10, 2, "    ");
}

void test_base64(){
    std::fstream f, f2;
    f.open("/home/wty/sylar_learn/root/xxx.jpg", std::ios::in|std::ios::binary);
    f.seekg(0, std::ios_base::end);
    std::streampos sp = f.tellg();
    int size = sp;
    std::cout << size << std::endl;
    char* buffer = (char*)malloc(sizeof(char)*size);
    f.seekg(0, std::ios_base::beg);//把文件指针移到到文件头位置

    f.read(buffer,size);

    std::cout << "file size:" << size << std::endl;
    std::string imgBase64 = sylar::base64_encode(buffer, size);
    std::cout << "img base64 encode size:" << imgBase64.size() << std::endl;
    std::string imgdecode64 = sylar::base64_decode(imgBase64);
    std::cout << "img decode size:" << imgdecode64.size() << std::endl;
    f2.open("out.jpg", std::ios::out|std::ios::binary);
    f2 << imgdecode64;
    f2.close();

}

std::string boundary = "----WebKitFormBoundaryNeRgAyg0BABC8eyb";

void test_opencv(){
    cv::Mat mat = cv::imread("/home/wty/sylar_learn/root/xxx.jpg");
    cv::namedWindow("img",0);
    cv::resizeWindow("img", 640, 480);
    cv::imshow("img", mat);
    cv::waitKey(0);
}

int main(int argc, char** argv){
//    test_base64();
    test_opencv();
    return 0;
}