#include"sylar/uri.h"
#include"sylar/log.h"

int main(int argc, char**argv){
    sylar::Uri::ptr ur = sylar::Uri::Create("http://www.baidu.com/test/uri?id=100&name=aa#frag");
    std::cout<<ur->toString()<<std::endl;
    std::cout<<ur->createAddress()->toString()<<std::endl;
    return 0;
}

