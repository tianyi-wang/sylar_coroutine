//
// Created by wty on 22-7-6.
//
#include"sylar/hook.h"
#include"sylar/iomanager.h"
#include"sylar/log.h"
sylar::Logger::ptr logger = SYLAR_LOG_ROOT();

void test_sleep(){
    sylar::IOManager iom(1);
    iom.schedule([](){
        sleep(2);
        SYLAR_LOG_INFO(logger)<<"sleep 2";
    });
    iom.schedule([](){
        sleep(3);
        SYLAR_LOG_INFO(logger)<<"sleep 3";
    });
    SYLAR_LOG_INFO(logger)<<"test hook";
}



int main(){
     test_sleep();



    return 0;
}