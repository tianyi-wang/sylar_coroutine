#include"sylar/sylar.h"
#include<iostream>
#include<sstream>
#include<thread>

sylar::Logger::ptr logger = SYLAR_LOG_ROOT();

int count = 0;
sylar::RWMutex mutex1;
sylar::Mutex mutex2;
void fun1(){

    SYLAR_LOG_INFO(logger) << "name: "<< sylar::Thread::GetName()
                             << " this.name:" << sylar::Thread::GetThis()->getName()
                             << " id: " << sylar::GetThreadId()
                             << " this id: " << sylar::Thread::GetThis()->getId();
    sylar::Mutex::Lock lock(mutex2);
    for(int i=0;i<100000;i++){
        count++;
//        SYLAR_LOG_INFO(logger) << "name: "<< sylar::Thread::GetName()<<" count="<<count;
    }
}

void test_static(){
    static int a = 0;
    a += 10;
    std::cout<<a<<std::endl;
}


int main(int argc, char** argv){
    SYLAR_LOG_INFO(logger) << "thread test1";
    std::vector<sylar::Thread::ptr> thrs;
    for(int i=0;i<5;i++){
        sylar::Thread::ptr thr1(new sylar::Thread(fun1,  "name_"+std::to_string(i)));
        thrs.push_back(thr1);
    }

    for(int i=0;i<thrs.size();++i){
        thrs[i]->join();
    }
    SYLAR_LOG_INFO(logger) << "count="<<count;

    return 0;
}