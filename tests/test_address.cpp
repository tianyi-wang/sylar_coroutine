#include "sylar/address.h"
#include "sylar/sylar.h"
#include "sylar/socket.h"

sylar::Logger::ptr logger = SYLAR_LOG_ROOT();

void test(){
    std::vector<sylar::Address::ptr> addrs;
    bool v = sylar::Address::Lookup(addrs, "www.baidu.com:ftp");
    if(!v){
        SYLAR_LOG_ERROR(logger)<<"Lookup fail";
    }
    SYLAR_LOG_INFO(logger)<<"addres size:"<<addrs.size();
    for(auto addr : addrs){
        SYLAR_LOG_INFO(logger)<<addr->toString();
    }
}

void test_interface(){
    std::multimap<std::string, std::pair<sylar::Address::ptr, uint32_t>> results;
    bool v = sylar::Address::GetInterfaceAddresses(results);
    if(!v){
        SYLAR_LOG_ERROR(logger)<<"failed";
    }
    for(auto  i : results){
        SYLAR_LOG_INFO(logger)<< i.first << " - " << i.second.first << " - " <<
                                i.second.first->toString();
    }
}

void test_ipv4(){
    sylar::IPAddress::ptr addr = sylar::IPAddress::Create("www.baidu.com");
    if(!addr){
        SYLAR_LOG_ERROR(logger)<<"failed";
        return;
    }
    SYLAR_LOG_INFO(logger)<<addr->toString();
}

int count(uint32_t num){
    int result = 0;
    while(num!=0){
        num &= num-1;
        result++;
    }
    return result;
}

int main(){
    // test_ipv4();
    // test();
     test_interface();

    return 0;
}