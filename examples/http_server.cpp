//
// Created by wty on 22-7-22.
//
#include"sylar/http/http_server.h"
#include"sylar/log.h"
#include<sstream>
#include<fstream>
#include <sys/mman.h>   // mmap PORT_READ

static sylar::Logger::ptr logger = SYLAR_LOG_ROOT();

int32_t setFile(const char* ch, sylar::http::HttpResponse::ptr rsp) {
    struct stat m_file_stat;
    if (stat(ch, &m_file_stat) < 0) {
        SYLAR_LOG_ERROR(logger) << "No Recourse";
        return 1;
    }
    else if (!(m_file_stat.st_mode & S_IROTH)){
        SYLAR_LOG_ERROR(logger) << "Recourse Cannot Read";
        return 2;
    }
    else if (S_ISDIR(m_file_stat.st_mode)) {
        SYLAR_LOG_ERROR(logger) << "Bad Recourse";
        return 3;
    }
    else {
        int fd = open(ch, O_RDONLY);
        rsp->setBody((char*)mmap(0, m_file_stat.st_size, PROT_READ, MAP_PRIVATE, fd, 0));
        rsp->setHeader("content-length", std::to_string(m_file_stat.st_size));
    }
    return 0;
}

int32_t show_pic(sylar::http::HttpRequest::ptr req
        ,sylar::http::HttpResponse::ptr rsp
        ,sylar::http::HttpSession::ptr session){
    
//    setFile("/home/wty/sylar_learn/root/picture.html", rsp);
    std::ifstream file("/home/wty/sylar_learn/root/picture.html");
    std::stringstream buffer;
    buffer<<file.rdbuf();
    rsp->setBody(buffer.str());

    return 0;
}

int32_t pic(sylar::http::HttpRequest::ptr req
        ,sylar::http::HttpResponse::ptr rsp
        ,sylar::http::HttpSession::ptr session){
    
//    setFile("/home/wty/sylar_learn/root/picture.html", rsp);
    std::ifstream file("/home/wty/sylar_learn/root/xxx.jpg");
    std::stringstream buffer;
    buffer<<file.rdbuf();
    rsp->setBody(buffer.str());

    return 0;
}

int32_t upload1(sylar::http::HttpRequest::ptr req
        ,sylar::http::HttpResponse::ptr rsp
        ,sylar::http::HttpSession::ptr session){
    
//    setFile("/home/wty/sylar_learn/root/picture.html", rsp);
    std::ifstream file("/home/wty/sylar_learn/root/upload_pic.html");
    std::stringstream buffer;
    buffer<<file.rdbuf();
    rsp->setBody(buffer.str());

    return 0;
}

int32_t upload2(sylar::http::HttpRequest::ptr req
        ,sylar::http::HttpResponse::ptr rsp
        ,sylar::http::HttpSession::ptr session){
    
//    setFile("/home/wty/sylar_learn/root/picture.html", rsp);
    std::ifstream file("/home/wty/sylar_learn/root/upload_pic2.html");
    std::stringstream buffer;
    buffer<<file.rdbuf();
    rsp->setBody(buffer.str());

    return 0;
}

int32_t upload3(sylar::http::HttpRequest::ptr req
        ,sylar::http::HttpResponse::ptr rsp
        ,sylar::http::HttpSession::ptr session){
    
//    setFile("/home/wty/sylar_learn/root/picture.html", rsp);
    std::ifstream file("/home/wty/sylar_learn/upload_pic/index.html");
    std::stringstream buffer;
    buffer<<file.rdbuf();
    rsp->setBody(buffer.str());

    return 0;
}

int32_t upload4(sylar::http::HttpRequest::ptr req
        ,sylar::http::HttpResponse::ptr rsp
        ,sylar::http::HttpSession::ptr session){
    
//    setFile("/home/wty/sylar_learn/root/picture.html", rsp);
    std::ifstream file("/home/wty/sylar_learn/root/upload4.html");
    std::stringstream buffer;
    buffer<<file.rdbuf();
    rsp->setBody(buffer.str());

    return 0;
}


int32_t php_server(sylar::http::HttpRequest::ptr req
        ,sylar::http::HttpResponse::ptr rsp
        ,sylar::http::HttpSession::ptr session){
    
    setFile("/home/wty/sylar_learn/upload_pic/sever.php", rsp);
    // std::ifstream file("/home/wty/sylar_learn/upload_pic/jquery-3.3.1.js");
    // std::stringstream buffer;
    // buffer<<file.rdbuf();
    // rsp->setBody(buffer.str());

    return 0;
}

int32_t bootstrap(sylar::http::HttpRequest::ptr req
        ,sylar::http::HttpResponse::ptr rsp
        ,sylar::http::HttpSession::ptr session){
    
    setFile("/home/wty/sylar_learn/upload_pic/bootstrap.css", rsp);
    // std::ifstream file("/home/wty/sylar_learn/upload_pic/bootstrap.css");
    // std::stringstream buffer;
    // buffer<<file.rdbuf();
    // rsp->setBody(buffer.str());

    return 0;
}


void run(){
    sylar::http::HttpServer::ptr server(new sylar::http::HttpServer(false));
    sylar::Address::ptr addr = sylar::Address::LookupAny("0.0.0.0:8000");
    while(!server->bind(addr)){
        sleep(1);
    }
    auto serv_dipatch = server->getServletDispatch();
    serv_dipatch->addServlet("/hello/xx", [](sylar::http::HttpRequest::ptr request
            , sylar::http::HttpResponse::ptr response
            , sylar::http::HttpSession::ptr session)->int32_t{

        response->setBody(request->toString());
        return 0;
    });

    serv_dipatch->addServlet("/pic", show_pic);
    serv_dipatch->addServlet("/xxx.jpg", pic);
    serv_dipatch->addServlet("/upload1", upload1);
    serv_dipatch->addServlet("/upload2", upload2);
    serv_dipatch->addServlet("/upload3", upload3);

    serv_dipatch->addServlet("/upload", upload4);

    serv_dipatch->addServlet("/server.php", php_server);
    serv_dipatch->addServlet("/bootstrap.css", bootstrap);
    server->start();
}

int main(int argc, char** argv){
    sylar::IOManager iom(2);
    iom.schedule(run);
}
